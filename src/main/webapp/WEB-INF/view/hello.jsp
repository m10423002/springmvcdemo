<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Hello Page</title>
	<c:url var="cssUrl" value="/res/css/style.css" />
	<link rel="stylesheet" type="text/css" href="${cssUrl}" />
</head>
<body>
	<h2>Spring MVC Course</h2>
	Your Name : ${param.myName}<br><br>
	The message : ${msg}
</body>
</html>