<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Student Form</title>
	<style>
		.error {color : red}
	</style>
</head>
<body>
	<form:form action="processForm" modelAttribute="myStudent">
		First Name : <form:input path="firstName"/><br><br>
		
		Last Name : <form:input path="lastName"/><br><br>
		
		Country : <form:select path="country" items="${countryOptions}" />
							
		<br><br>
		
		Skilled Language : 
		<form:radiobutton path="language" value="Chinese"/> Chinese				
		<form:radiobutton path="language" value="English"/> English				
		<form:radiobutton path="language" value="Japanese"/> Japanese				
		<br><br>

		Hobbies : 
		<form:checkbox path="hobbies" value="sport"/> Sport
		<form:checkbox path="hobbies" value="movie"/> Movie
		<form:checkbox path="hobbies" value="cuisine"/> Cuisine
		<br><br>
		
		Course Code : <form:input path="courseCode"/>
		<form:errors path="courseCode" cssClass="error" />
		<br><br>
		
		<input type="submit" > 
	</form:form>

</body>
</html>