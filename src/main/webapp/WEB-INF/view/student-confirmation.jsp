<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Student Confirmation</title>
</head>
<body>
	
	Student is confirmed : ${myStudent.firstName} ${myStudent.lastName}
	<br><br>
	
	Country : ${myStudent.country}
	<br><br>
	
	Skilled Language : ${myStudent.language}
	<br><br>
	
	Hobbies:
	<ul>
		<c:forEach var="hobby" items="${myStudent.hobbies}">
			<li>${hobby}</li>
		</c:forEach>
	</ul> 
	<br><br>
	
	Course Code : ${myStudent.courseCode}
	<br><br>

	
</body>
</html>