package com.bill.springdemo.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {
	
	// a method to show form
	@RequestMapping("/showForm")
	public String showForm() {
		return "hello-form";
		
	}
	
	// a method to process form
	@RequestMapping("/processForm")
	public String processForm() {
		return "hello";
		
	}
	
	// a method to read form data and add data to the model
	@RequestMapping("/processFormVersion2")
	public String shout(HttpServletRequest request, Model model) {
		
		//read the request parameter from html form
		String name = request.getParameter("myName");
		
		//convert data to all caps
		name = name.toUpperCase();

		//create message
		String msg = "Yo ! " + name;

		//add message to the model
		model.addAttribute("msg", msg);
		
		return "hello";
	}
	
	@RequestMapping("/processFormVersion3")
	public String shout2(
			@RequestParam("myName") String name, 
			Model model) {
		
		//convert data to all caps
		name = name.toUpperCase();
		
		//create message
		String msg = "Hey ! " + name;
		
		//add message to the model
		model.addAttribute("msg", msg);
		
		return "hello";
	}
}
