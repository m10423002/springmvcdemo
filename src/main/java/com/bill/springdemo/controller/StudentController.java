package com.bill.springdemo.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bill.springdemo.model.Student;

@Controller
@RequestMapping("/student")
public class StudentController {
	
	Logger log = LoggerFactory.getLogger(StudentController.class);
	
	@Value("#{countryOptions}")
	private LinkedHashMap<String, String> countryOptions;
	
	@RequestMapping("/showForm")
	public String showForm(Model model) {
		//create a student object
		Student student = new Student();
		
		//add object to the model
		model.addAttribute("myStudent", student);
		model.addAttribute("countryOptions", countryOptions);
		
		return "student-form";
	}
	
	@RequestMapping("/processForm")
	public String processForm(
			@Valid @ModelAttribute("myStudent") Student theStudent,
			BindingResult bindingResult) {
		
		log.info("theStudent : {}, {} ", theStudent.getFirstName(), theStudent.getLastName());
		
		if(bindingResult.hasErrors()) {
			return "student-form";
		
		}else {			
			return "student-confirmation";
		}
				
	}
}
