package com.bill.springdemo.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CourseCodeConstraintValidator implements ConstraintValidator<CourseCode, String> {

	private String coursePrefix;
	
	@Override
	public void initialize(CourseCode courseCode) {		
		coursePrefix = courseCode.value();
	}

	@Override
	public boolean isValid(String theCourseCode, ConstraintValidatorContext constraintValidatorContext) {
		boolean result = false;
		
		if(theCourseCode != null) {		
			result = theCourseCode.startsWith(coursePrefix);
		}else {
			result = true;
		}
				
		return result;
	}

}
