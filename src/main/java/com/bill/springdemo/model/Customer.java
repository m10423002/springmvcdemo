package com.bill.springdemo.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.bill.springdemo.validation.CourseCode;

public class Customer {

	private String firstName;
	
	@NotNull(message = "該項目為必填")
	@Size(min = 1, message = "is required")
	private String lastName;
	
	@NotNull(message = "該項目為必填")
	@Min(value = 0, message = "must be greater than or equal to 0")
	@Max(value = 10, message = "must be less than or equal to 10")
	//@Pattern(regexp = "[1]{1}[0]{1}|\\b[0-9]{1}\\b", message = "必須為介於1-10的數值")
	private Integer freePasses;
	
	@NotNull(message = "該項目為必填")
	@Pattern(regexp = "[a-zA-Z0-9]{5}", message = "郵遞區號需五碼")
	private String postalCode;
	
	@CourseCode
	private String courseCode;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}	
	public Integer getFreePasses() {
		return freePasses;
	}
	public void setFreePasses(Integer freePasses) {
		this.freePasses = freePasses;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCourseCode() {
		return courseCode;
	}
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}	
	
}
