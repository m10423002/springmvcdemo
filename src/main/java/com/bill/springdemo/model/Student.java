package com.bill.springdemo.model;

import java.util.LinkedHashMap;

import com.bill.springdemo.validation.CourseCode;

public class Student {

	private String firstName;
	private String lastName;
	private String country;
	private String language;
	private LinkedHashMap<String, String> countryOptions;
	private String[] hobbies;
	
	@CourseCode
	private String courseCode;
	
	public Student() {
		/**
		countryOptions = new LinkedHashMap<String, String>();
		countryOptions.put("FR", "France");
		countryOptions.put("JP", "Japan");
		countryOptions.put("CN", "China");
		countryOptions.put("CA", "Canada");		
		*/
	}
	
	
	public Student(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}

	public LinkedHashMap<String, String> getCountryOptions() {
		return countryOptions;
	}


	public String getLanguage() {
		return language;
	}


	public void setLanguage(String language) {
		this.language = language;
	}


	public String[] getHobbies() {
		return hobbies;
	}


	public void setHobbies(String[] hobbies) {
		this.hobbies = hobbies;
	}


	public String getCourseCode() {
		return courseCode;
	}


	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}	
	
	
	
}
